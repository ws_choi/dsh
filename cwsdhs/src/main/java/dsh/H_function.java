package dsh;

import java.io.Serializable;

import point.Point;


public class H_function implements Serializable {
	
	private static final long serialVersionUID = -2385386848369770422L;
	
	double[] a;
	double[] avg;
	
	public H_function(double[] a, double[] avg) {
		
		this.a = a.clone();
		this.avg = avg.clone();
		
	}
	
	public int hash (Point point){
	
		double output = 0;
		
		double[] input_vec = point.vector.clone();
		
		for (int i = 0; i < input_vec.length; i++) {
			input_vec[i] -= avg[i];
		}
		
		for (int i = 0; i < input_vec.length; i++) {
			output += input_vec[i] * a[i];
		}
		
		
		return (output > 0 ) ? 1:0;
	}
	
	public String toString (){
		
		String buf= new String();
		
		buf+=a[0];
		
		for (int i = 1; i < a.length; i++) {
			buf += ", "+a[i];
		}
		return "vector: " + buf;
	}

}
