package dsh;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import point.Point;
import point.data.Data;

public class WeightMatrix {
	//TODO
	public double[][] matrix;
	int k, ck;
	java.util.List<Point> querySet;
	java.util.List<Data>  dataSet;

	
	
	public WeightMatrix(int k, double delta,  List<Point> querySet, List<Data>  dataSet) {

		this.querySet = querySet;
		this.dataSet = dataSet;
		this.k=k;
		this.ck= (int)(k/delta);
		
		matrix = new double[querySet.size()][dataSet.size()];
		
		for (int i = 0; i < querySet.size(); i++) {
			
			Point query = querySet.get(i);
			ArrayList<Data> knn_objs = nearest_objs(ck, query, dataSet);
			
			for (int j = 0; j < k; j++) {
				
				int target_index = dataSet.indexOf(knn_objs.get(j));
				matrix[i][target_index] = 1;
				
			}
			
			
			for (int j = k; j < ck; j++) {
				
				int target_index = dataSet.indexOf(knn_objs.get(j));
				matrix[i][target_index] = 0;
				
			}

			int the_num_of_query = querySet.size();
			int the_num_of_nonck = the_num_of_query - ck;

			if(the_num_of_nonck < k)
				for (int j = ck; j < the_num_of_query ; j++) 
					matrix[i][j] = -1;
				
			else{
				
				int num = 0;
				
				while(num < ck){
					
					int random_j = ck + (int)(Math.random()*the_num_of_nonck);
					
					if(matrix[i][random_j] == -1) continue;
					else {
						matrix[i][random_j] = -1;
						num++;
					}
					
				}	
			}

		}
		
		
	}
	
	private ArrayList<Data> nearest_objs ( int ck, Point query, Collection<? extends Data> dataset)
	{

		final Point my_query = query;

		ArrayList<Data> sortedSet = new ArrayList<Data>(dataset);
		
		Collections.sort (sortedSet,
					
				new Comparator<Data>() {

				public int compare(Data o1, Data o2) {
				
					double dist_1 = dist_sqr(my_query, o1);
					double dist_2 = dist_sqr(my_query, o2);
					
					if(dist_1>dist_2)
						return 1;
					
					if(dist_1<dist_2)
						return -1;
					
					else return 0;
					
				}
			}
				
		);
		
		ArrayList<Data> result = new ArrayList<Data>();
		
		for (int i = 0; i < ck; i++) {
			result.add(sortedSet.remove(0));
		}
		
		return result;
	}

	
	private double dist_sqr (Point query, Data obj)
	{
		double dist = 0;
		int dim = query.dimension;

		double[] q_vec=query.vector;
		double[] obj_vec=obj.vector;
		
		for (int i = 0; i < dim ; i++) {
			dist += Math.pow(q_vec[i] - obj_vec[i], 2);
		}
		
		return dist;
	}
	
	
	public double[][] getMatrix()
	{
		return matrix;
	}
}
