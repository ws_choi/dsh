package dsh;

import java.io.Serializable;
import java.util.ArrayList;

import point.Point;
import point.data.Data;

public class G_HashTable implements Serializable{

	ArrayList<Data>[] tables;
	
	private static final long serialVersionUID = 1L;
	
	G_funciton g_function;
	public G_HashTable(G_funciton g_function) {
		
		this.g_function = g_function;
		tables = new ArrayList[ (int)Math.pow(2, g_function.m) ];
	}


	public synchronized void put(Point point, Data data) {
		
		int hash_index = get_hash_index(point);
		
		if(tables[hash_index] == null) tables[hash_index] = new ArrayList<Data>();
		
		tables[hash_index].add(data);
		
	}


	private int get_hash_index(Point point) {

		String buf ="";
		int[] hash_2 = g_function.hash(point);

		for (int i = 0; i < hash_2.length; i++) buf+=hash_2[i];
		
		return Integer.parseInt(buf, 2);
		
	}
	
	public synchronized ArrayList<Data> get(Point point) {
		
		int hash_index = get_hash_index(point);
		
		if(tables[hash_index] == null) return null;
		
		return tables[hash_index];
		
	}
	
	
	public void Write(){
		
		

	}

}
