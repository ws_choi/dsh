package dsh;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import point.Point;
import point.data.Data;

public class DSH implements Serializable{

	private static final long serialVersionUID = 5475393011618660340L;

	LinkedList<H_function> h_Family;
	public ArrayList<G_funciton> g_functions;
	public ArrayList<G_HashTable> g_tables;
	
	int k, m, l, itrCounter;
		
	public DSH(int k, double alpha, double delta, double p1, double p2, List<Point> querySet, List<Data> dataSet) {
		
		this.k=k;
		auto_setParameter(delta, p1, p2);
		itrCounter = m*l*2;
		
		h_Family = (new H_Family_Builder(k, itrCounter, alpha, delta, p1, p2, querySet, dataSet)).get_H_Family();
		g_functions = new ArrayList<G_funciton>();
		g_tables = new ArrayList<G_HashTable>();
		
		for (int i = 0; i < l; i++) {

			H_function[] h_concat = new H_function[m];
			
			for (int j = 0; j < m; j++) 
				h_concat[j] = h_Family.remove((int)(Math.random()*h_Family.size()));

			G_funciton x = new G_funciton(h_concat);
			g_functions.add( x );
			
		}

		for (Iterator<G_funciton> iterator = g_functions.iterator(); iterator.hasNext();) {

			
			G_HashTable table = new G_HashTable(iterator.next());
	
			for (Iterator<Data> data_iter = dataSet.iterator(); data_iter.hasNext();) {
				Data data = data_iter.next();
				table.put(data, data);
			}
			
			g_tables.add(table);
			
		} 

	}
	
	
	@SuppressWarnings("unchecked")
	public DSH(String string) throws FileNotFoundException, IOException, ClassNotFoundException {
		
		@SuppressWarnings("resource")
		ObjectInputStream input = new ObjectInputStream(new BufferedInputStream(new FileInputStream(string)));
		k = input.readInt();
		g_functions = (ArrayList<G_funciton>) input.readObject();
		g_tables= (ArrayList<G_HashTable>)input.readObject();
		
	}

	public DSH (int k, double sampling_rate, double alpha, double delta, double p1, double p2, List<Data> dataSet){
		this.k=k;
		auto_setParameter(delta, p1, p2);
		itrCounter = m*l*2;

		h_Family = (new H_Family_Builder(k, sampling_rate, itrCounter, alpha, delta, p1, p2, dataSet)).get_H_Family();
		g_functions = new ArrayList<G_funciton>();
		g_tables = new ArrayList<G_HashTable>();
		
		for (int i = 0; i < l; i++) {

			H_function[] h_concat = new H_function[m];
			
			for (int j = 0; j < m; j++) 
				h_concat[j] = h_Family.remove((int)(Math.random()*h_Family.size()));

			G_funciton x = new G_funciton(h_concat);
			g_functions.add( x );
			
		}

		for (Iterator<G_funciton> iterator = g_functions.iterator(); iterator.hasNext();) {

			
			G_HashTable table = new G_HashTable(iterator.next());
	
			for (Iterator<Data> data_iter = dataSet.iterator(); data_iter.hasNext();) {
				Data data = data_iter.next();
				table.put(data, data);
			}
			
			g_tables.add(table);
			
		} 

	}


	private void auto_setParameter(double delta, double p1, double p2) {
		
		m=1; l=1; double p2_m = p2;
		while(p2_m > 0.001){	p2_m *= p2;	m++; }	
		while(1-Math.pow((1-Math.pow(p1, m)), l) < delta) l++;
					
	}




	public Data[] k_Nearest_Neighbor (Point query){
		
		final Point my_query_point = query;
		ArrayList<Data> candidate_set = new ArrayList<Data>();
		
		for (Iterator<G_HashTable> gtable_iter = g_tables.iterator(); gtable_iter.hasNext();) {
			G_HashTable g_HashTable =  gtable_iter.next();

			ArrayList<Data> res = g_HashTable.get(query);
			if(res!=null)candidate_set.addAll(res);
			
		}
		
		
		Collections.sort (candidate_set,
				
				new Comparator<Data>() {

				public int compare(Data o1, Data o2) {
				
					double dist_1 = dist_sqr(my_query_point , o1);
					double dist_2 = dist_sqr(my_query_point, o2);
					
					if(dist_1>dist_2)
						return 1;
					
					if(dist_1<dist_2)
						return -1;
					
					else return 0;
					
				}

				private double dist_sqr (Point query, Data obj)
				{
					double dist = 0;
					int dim = query.dimension;
			
					double[] q_vec=query.vector;
					double[] obj_vec=obj.vector;
					
					for (int i = 0; i < dim ; i++) {
						dist += Math.pow(q_vec[i] - obj_vec[i], 2);
					}
					
					return dist;
				}
			}
				
		);
		
		print_size_of_candidates(candidate_set);
		int cnt=0;
		
		ArrayList<Data> res = new ArrayList<Data>();
		
		Data prev_data = null;
		
		for (int i = 0; i < candidate_set.size() && cnt < k ; i++) {
			Data data = (Data) candidate_set.get(i);
			if(data.equals(prev_data)) continue;
			
			prev_data = data;
			
			res.add(data);
			cnt++;
		}

		
		return res.toArray(new Data[0]);
	}

	
	private void print_size_of_candidates(ArrayList<Data> candidate_set) {
		
		int cnt = 0;
		Data pre=null;
		
		for (Data data : candidate_set) 
		{
			if (!data.isSame(pre)) cnt++;
			pre = data;
		}
		
		System.out.println("size of candidate set: " +cnt);
		
	}


	public void toFile(String filename) throws IOException {
		
		ObjectOutputStream output = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(filename)));
		
		output.writeInt(k);
		output.writeObject(g_functions);
		output.writeObject(g_tables);

		output.close();
		
	}
	

}
