package dsh;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.jblas.DoubleMatrix;
import org.jblas.Eigen;

import point.Point;
import point.data.Data;


public class H_Family_Builder {
	int k, ck;
	int dim, n, q;
	int t;
	
	double alpha, delta, p1, p2;
	double[] avg;

	List<Point> querySet;
	List<Data> dataSet;
	
	LinkedList<H_function> h_family;
	
	//TODO
	public WeightMatrix wm;
	DoubleMatrix W, X, D,Q, D_prime;
	DoubleMatrix A,B;
	
	int[][] fixed;


	
	public H_Family_Builder(int k, int itrCounter, double alpha, double delta, double p1, double p2, 
							List<Point> querySet, List<Data> dataSet) {
	
		this.k=k; 
		this.alpha = alpha; this.delta = delta; 
		this.p1 = p1; this.p2 = p2; 
		this.querySet=querySet; this.dataSet=dataSet;
		this.ck=(int) (k/delta);
		this.t = 0;
		
		build(itrCounter);
		
	}

	public H_Family_Builder(int k, double sampling_rate, int itrCounter, 
			double alpha, double delta, double p1, double p2, List<Data> dataSet) {
		
		this.k=k; 
		this.alpha = alpha; this.delta = delta; 
		this.p1 = p1; this.p2 = p2; 
		this.querySet= new ArrayList<Point>();
		this.dataSet=dataSet;
		random_sampling(sampling_rate, querySet, dataSet);
		this.ck=(int) (k/delta);
		this.t=0;
		
		build(itrCounter);

	}

	private void build(int itrCounter) {
		
		wm = new WeightMatrix(k,  delta, querySet, dataSet);
				h_family = new LinkedList<H_function>();
		matrix_building( querySet,  dataSet);
		
		
		do
		{

			DoubleMatrix min_eigen_vector = compute_h();
			H_function new_h = new H_function(min_eigen_vector.toArray(), avg);
			h_family.add(new_h);
			update_W(W, new_h);
			update_D(W.toArray2());
			update_D_prime(W.toArray2());
			t++;
		}while(  exists_bad_case() || t < itrCounter  );
	}

	private void random_sampling(double sampling_rate, List<Point> querySet,
			List<Data> dataSet) {
		
		for (Data data : dataSet) 
			if(Math.random()<sampling_rate) querySet.add(data);
		
	}

	private void update_W(DoubleMatrix w2, H_function new_h) {
		
		double[][] old_wm = w2.toArray2();
		
		for (int i = 0; i < old_wm.length; i++) 
			for (int j = 0; j < old_wm[0].length; j++) 
				if(wm.matrix[i][j] > 0)
					old_wm [i][j] *= Math.pow(alpha, p1-1 + varPHI(new_h, querySet.get(i), dataSet.get(j))); 
				else if (wm.matrix[i][j] < 0)
					{
//					if(varPHI(new_h, querySet.get(i), dataSet.get(j))==0)
//					System.out.println("before "+old_wm[i][j]);
					old_wm [i][j] *= Math.pow(alpha, -1 * (p2-1 + varPHI(new_h, querySet.get(i), dataSet.get(j))));
//					if(varPHI(new_h, querySet.get(i), dataSet.get(j))==0)
//					System.out.println("after "+old_wm[i][j]);
					}
		
		W = new DoubleMatrix(old_wm);
		
			

	}

	private double varPHI(H_function new_h, Point point, Data data) {

		return (new_h.hash(point) == new_h.hash(data)) ? 0:1;

	/*	if(new_h.hash(point) == new_h.hash(data)) return 0;
		else return 1;
	*/	
	}

	private boolean exists_bad_case() {
	
		int badcase = 0;
		double[][] w_0 = wm.matrix.clone();
		double[][] w_t = W.toArray2();

		for (int i = 0; i < w_0.length; i++) 
			for (int j = 0; j < w_0[0].length; j++) 
				if(Math.abs(w_t[i][j]) > Math.abs(w_0[i][j])){
				//	System.out.println("bad: i: " + i + ", j: " + j +", fixed: " +fixed[i][j]++	);
				//	System.out.println("��----case: "+w_t[i][j] +"and "+ w_0[i][j]);
					badcase ++;
				}

				
		System.out.println("# of bad case: " + badcase +", " + "# of pairs: " + w_0.length*(k+ck));

		//System.out.println((int)(0.01*w_0.length * w_0[0].length));
		if(badcase > (int)(0.01 * w_0.length * w_0[0].length)) return true;
		
		

		return false;
	}

	private DoubleMatrix compute_h() {

		A = (( X.mmul(D).sub(Q.mmul(W)) ).mmul(X.transpose()))
				.add (( Q.mmul(D_prime).sub(X.mmul( W.transpose() ) )).mmul(Q.transpose()));
		B = X.mmul(X.transpose());
		
		DoubleMatrix[] eigen_vectors = Eigen.symmetricGeneralizedEigenvectors(A, B);
		return eigen_vectors[0].getRow( find_min_index(eigen_vectors[1].toArray())  );
	}

	private int find_min_index(double[] es) {

		double min = Double.MAX_VALUE;
		int min_index = -1;
		
		for (int i = 0; i < es.length; i++) {
			if(min > es[i])
			{
				min = es[i];
				min_index=i;
			}
		}
	
		return min_index;
		
	}

	private void matrix_building(List<Point> querySet, List<Data> dataSet) {
		
		dim = querySet.get(0).dimension;
		n = dataSet.size();
		q = querySet.size();
		
		avg = new double[dim];
		
		double[][] query_set = new double[dim][querySet.size()];
		double[][] data_set = new double[dim][dataSet.size()];
				
		init_arrays(querySet, dataSet, dim, query_set, data_set);
		normalize(data_set, query_set);
		
		Q = new DoubleMatrix(query_set);
		X = new DoubleMatrix(data_set);
		W = new DoubleMatrix(wm.matrix);
		
		update_D(W.toArray2());
		update_D_prime(W.toArray2());
		
		fixed = new int[q][n];
	}

	private void update_D(double[][] w) {
		
		double[][] d_array = new double[n][n];
		
		for (int j = 0; j < n; j++) {

			double sum = 0;
			for (int i = 0; i < q; i++) {
				sum+=w[i][j];
			}
			d_array[j][j] = sum;
		}
		
		D = new DoubleMatrix(d_array);
	}

	private void update_D_prime(double[][] w) {
		
		double[][] d_prime_array = new double[q][q];
		
		for (int i = 0; i < q; i++) {

			double sum = 0;
			for (int j = 0; j < n; j++) {
				sum+=w[i][j];
			}
			d_prime_array[i][i] = sum;
		}
		D_prime = new DoubleMatrix(d_prime_array);	
	}

	private void init_arrays(List<Point> querySet, List<Data> dataSet, int dim,
							double[][] query_set, double[][] data_set) {

		for (int i = 0; i < querySet.size(); i++) {

			double[] vec = querySet.get(i).vector;
			
			for (int j = 0; j < dim; j++) 
				query_set[j][i] = vec[j];
			
		}
		
		for (int i = 0; i < dataSet.size(); i++) {

			double[] vec = dataSet.get(i).vector;
			
			for (int j = 0; j < dim; j++) 
				data_set[j][i] = vec[j];
			
		}
	}

	private void normalize(double[][] data_set, double[][] query_set) {
		
		int dim = data_set.length;
		int size_data = data_set[0].length;
		int size_query = query_set[0].length;
		
		avg = new double[dim];
		
		for (int i = 0; i < dim; i++) 
			for (int j = 0; j < size_data; j++) 
				avg[i] += data_set[i][j];
			
		
		for (int i = 0; i < dim; i++) 
			avg[i] /= size_data;

		for (int i = 0; i < dim; i++) 
		{
			for (int j = 0; j < size_data; j++)
				data_set[i][j] -= avg[i];

			for (int j = 0; j < size_query; j++)
				data_set[i][j] -= avg[i];

			
		}
			
			
		
		
	}
	
	public LinkedList<H_function> get_H_Family (){
		return h_family;
	}
	
	
}


/*		H_function h1 = new H_function(min_eigen_vector.toArray(), avg);

for (int i = 0; i < querySet.size(); i++) {
	
	int true_positive =0 ;
	int false_positive =0;
	int true_negative =0;
	int false_negative =0;
	
	for (int j = 0; j < dataSet.size(); j++) {
		
		if(wm.matrix[i][j] == 1)
		{
			if(h1.hash(querySet.get(i)) == h1.hash(dataSet.get(j))) true_positive++;
			else false_negative ++;
		}
		
		else if (wm.matrix[i][j] == -1)
		{
			if(h1.hash(querySet.get(i)) == h1.hash(dataSet.get(j))) false_positive++;
			else true_negative ++;
		}	
		
		
	}
	
	System.out.println("TP: " + true_positive+", FP: " + false_positive+", TN: " + true_negative+", FN: " + false_negative);
	
}*/
