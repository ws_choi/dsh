package dsh;

import java.io.Serializable;

import point.Point;

public class G_funciton implements Serializable {

	private static final long serialVersionUID = 8423752585415931530L;

	H_function[] h_functions;
	int m;
	
	public G_funciton(H_function[] h_functions) {
		this.h_functions = h_functions.clone();
		this.m = h_functions.length;
	}
	
	public int[] hash(Point point){
		int[] result = new int[m];
		for (int i = 0; i < result.length; i++) {
			result[i] = h_functions[i].hash(point);
		}
		
		return result;
	}
	
	public Integer[] hash_Wrapper(Point point){
		Integer[] result = new Integer[m];
		for (int i = 0; i < result.length; i++) {
			result[i] = new Integer(h_functions[i].hash(point));
		}
		
		return result;
	}
	
	public final static boolean isSame (int[] o1, int[] o2)
	{
		for (int i = 0; i < o2.length; i++) {
			if(o1[i] != o2[i]) return false;
		}
		return true;
	}
}
