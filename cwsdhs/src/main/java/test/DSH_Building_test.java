package test;

import io.handler.CsvParser;

import java.io.IOException;
import java.util.ArrayList;

import point.data.Data;
import dsh.DSH;

public class DSH_Building_test {

	public static void main(String[] args) throws NumberFormatException, IOException {
		
		Data data;
		CsvParser parser;
		
		ArrayList<Data> dataSet = new ArrayList<Data>();
		
		parser = new CsvParser("resources/normal 100 10folds.csv");		
		while((data = parser.getNext()) != null) dataSet.add(data);

		//param: DSH(k, sampling_rate alpha, delta, p1, p2, querySet, dataSet)
		DSH dsh = new DSH(10, 0.1, 1.5, 0.7, 0.9, 0.1, dataSet);
		
		dsh.toFile("resources/sample.dsh");
		
		Data[] res = dsh.k_Nearest_Neighbor(dataSet.get(0));
		for (int i = 0; i < res.length; i++) {
			System.out.println(res[i]);
		}	



		

	}
}
