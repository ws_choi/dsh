package point.data;

import java.io.Serializable;

import point.Point;

public class Data extends Point implements Serializable{

	private static final long serialVersionUID = -6619439217020657589L;
	final static StringBuffer out = new StringBuffer();
	
	public Data(int dim, double[] vector) {
		super(dim, vector);
	}
	
	public Data(int dim)
	{
		super(dim);
		vector = new double[dimension];
	}
	
	public String toString(){
		
		out.delete(0, out.length());
		out.append(vector[0]);
		
		
		for (int i = 1; i < vector.length; i++)
			out.append(", "+ vector[i]);
		
		
		return out.toString();
		
	}

	public boolean isSame(Data other) {
		
		if(other == null) return false;
		
		double[] others = other.vector;
		for (int i = 0; i < vector.length; i++)
			if(vector[i] != others[i]) return false;
		
		return true;
	}
	
}
